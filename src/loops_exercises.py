from random import randint
import math

def print_hello_40_times():
    for i in range(1, 41):
        print('Hello', i)

#new
def print_positive_numbers_divisible_by_7(start, stop):
    for i in range(start, stop + 1):
        if i % 7 == 0:
            print(i)

#new
def print_n_random_numbers(n):
    for i in range(n):
        print(f'loop {i}:', randint(1, 100))

#new
def print_square_roots_of_numbers(list_of_numbers):
    for number in list_of_numbers:
        print(math.sqrt(number))

if __name__ == '__main__':
    print_hello_40_times()
    #print_positive_numbers_divisible_by_7(1, 100)
    print_n_random_numbers(20)
    print(math.sqrt(9))
    print(math.sqrt(16))
    list_of_measurment_result = [11.0, 123, 65, 98]
    print_square_roots_of_numbers(list_of_measurment_result)







