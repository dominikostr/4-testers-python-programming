first_name = "Dominik"
last_name = "Ostrowski"
age = 25

print(first_name)
print(last_name)
print(age)

# Below I describe my friendship details as a set of variables

friend_name = 'Wojtek'
friend_age = 25
friend_number_of_animals = 2
friend_has_driving_license = True
friendship_duration_in_years = 5.5

print("Friend's name:", friend_name, sep="\t")

print(friend_name, friend_age, friend_number_of_animals, friend_has_driving_license, friendship_duration_in_years, sep=' , ')
