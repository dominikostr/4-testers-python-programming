def calculate_the_square_of_number(input_number):
    return input_number ** 2


def calculate_the_volume_of_cuboid(a, b, c):
    return a * b * c


def calculate_how_many_farenheit_degree_is(celcius_degree):
    return celcius_degree * 1.8 + 32


if __name__ == '__main__':
    # zdanie 1
    square_1 = calculate_the_square_of_number(0)
    square_2 = calculate_the_square_of_number(16)
    square_3 = calculate_the_square_of_number(2.55)

    print(square_1)
    print(square_2)
    print(square_3)

    # zadanie 2
    print(calculate_how_many_farenheit_degree_is(20))

    # zadanie 3
    print(calculate_the_volume_of_cuboid(3, 5, 7))
