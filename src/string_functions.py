def print_greetings_for_a_person_in_the_city(name, city):
    print(f"Witaj {name}! Miło Cię widzieć w naszym mieście: {city}!")


def generate_emails_address_in_4_testers(name, lastname):
    print(f"{name.lower()}.{lastname.lower()}@4testers.pl")

if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city("Wojciech", "Gdańsk")
    print_greetings_for_a_person_in_the_city("Ola", "Warszawa")
    generate_emails_address_in_4_testers("Dominik", "Ostrowski")
   




