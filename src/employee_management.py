#new
import random
import time




def generate_random_email():
    domain = 'examples.com'
    names_list = ['James', 'Kate', 'Mark', 'Biil', 'Susan']
    random_name = random.choice(names_list)
    # suffix = random.randit(1, 1_000_000)
    suffix = time.time()
    return f'{random_name}.{suffix}@{domain}'

def generate_random_number():
    return random.randint(0, 40)

def generate_random_boolean():
    return random.choice([True, False])

def get_dictionary_of_employees():
    random_email = generate_random_email()
    random_number = generate_random_number()
    random_boolean = generate_random_boolean()
    return {
        'email' : random_email,
        'seniority_years' : random_number,
        'female' : random_boolean
    }

def generate_n_random_list_of_dictionaries(number_of_dictionaries):
    list_of_dictionaries = []
    for number in range(number_of_dictionaries):
        list_of_dictionaries.append(get_dictionary_of_employees())
    return list_of_dictionaries


if __name__ == '__main__':
    print(get_dictionary_of_employees())
    print(generate_n_random_list_of_dictionaries(25))


