import random
# lists for dictionaries
firstnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Eryka', 'James', 'Bob', 'Jan', 'Hans', 'Orestes']
surname = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina', 'Hilton']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Australia']


def generate_random_firstname():
    name = random.choice(firstnames)
    return name

def generate_random_surname():
    random_surname = random.choice(surname)
    return random_surname

def generate_random_countries():
    random_countries = random.choice(countries)
    return random_countries

def generate_random_age():
    random_age = random.randint(5,45)
    return random_age

def generate_random_adulty_info():
    if generate_random_age() >= 18:
        return 'true'
    else:
        return 'false'

def get_dictionary_of_people():
    random_firstname = generate_random_firstname()
    random_surname = generate_random_surname()
    random_country = generate_random_countries()
    random_age = generate_random_age()
    birth_year = 2022 - random_age
    random_adulty = generate_random_adulty_info()
    random_email = f'{random_firstname.lower()}.{random_surname.lower()}@example.com'
    person = {
        'firstname': random_firstname,
        'lastname': random_surname,
        'email': random_email,
        'age': random_age,
        'birt year': birth_year,
        'adulty': random_adulty,
        'country': random_country
    }
    print(f'Hi, I`m {random_firstname} {random_surname}. I come from {random_country} and I was born in {birth_year}')
    
    return person


def generate_n_random_list_of_dictionaries(number_of_dictionaries):
    list_of_dictionaries = []
    for number in range(number_of_dictionaries):
        list_of_dictionaries.append(get_dictionary_of_people())
    return list_of_dictionaries

if __name__ == '__main__':
    print(generate_n_random_list_of_dictionaries(10))



