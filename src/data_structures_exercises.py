import random
import string

# get random password pf length 8 with letters, digits, and symbols
characters = string.ascii_letters + string.digits + string.punctuation
password = ''.join(random.choice(characters) for i in range(8))
print("Random password is:", password)


def calculate_average_of_a_list(input_list):
    return sum(input_list) / len(input_list)


def calculate_sum_of_2_number(a, b):
    return (a + b) / 2


def generate_random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(characters) for i in range(8))


def generate_login_data_for_email(email):
    return generate_random_password()


if __name__ == '__main__':
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]
    january_average = calculate_average_of_a_list(january)
    february_average = calculate_average_of_a_list(february)
    biomonthly_average = calculate_sum_of_2_number(january_average, february_average)
    print(biomonthly_average)
    email1 = "user@exapmple.com"
